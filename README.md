# Dindin
Dindin is a sample API for hapi.js. It is a social network for food. 

It is based on the "building an API" chapter of hapi.js in Action by Matt Harrison.

- created using hapi.js
- sqlite is the database used
- sequelize and sqlite3 are the database/ORM modules used
- hapi-auth-basic is the authentication used
