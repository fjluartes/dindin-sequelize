'use strict';
const Models = require('../models');

exports.home = function(request, h) {
  return h.file('./public/index.html');
};

exports.find = function(request, h) {
  return Models.Recipe.findAll();
};

exports.findOne = function(request, h) {
  return Models.Recipe.findOne({
    where: {
      id: request.params.id
    }
  });
};

exports.users = function(request, h) {
  return Models.User.findAll();
};

exports.create = function(request, h) {
  return Models.Recipe.build({
    name: request.payload.name,
    cooking_time: request.payload.cooking_time,
    prep_time: request.payload.prep_time,
    serves: request.payload.serves,
    cuisine: request.payload.cuisine,
    ingredients: request.payload.ingredients,
    directions: request.payload.directions,
    stars: request.payload.stars,
    user_id: request.payload.user_id
  }).save().then(() => {
    return Models.Recipe.findOne({
      where: {
        name: request.payload.name
      }
    });
  }).catch((err) => {
    console.log(err);
    return err;
  });
};

exports.star = function(request, h) {
  return Models.Recipe.update({
    stars: request.payload.stars
  }, {
    where: {
      id: request.params.id
    }
  }).then(() => {
    return Models.Recipe.findOne({
      where: {
        id: request.params.id
      }
    });
  }).catch((err) => {
    console.log(err);
    return err;
  });
};
