'use strict';
const Bcrypt = require('bcrypt');
const Hapi = require('hapi');
const Models = require('./models');

const validate = async (request, username, password) => {
  if (username === 'help') {
    return { response: h.redirect('https://hapijs.com/help') };
  }

  const user = await Models.User.findOne({
    where: { username: username }
  });

  if (!user.get('username')) {
    return { credentials: null, isValid: false };
  }

  const isValid = await Bcrypt.compare(password, user.get('password'));
  const credentials = { id: user.id, name: user.name };

  return { isValid, credentials };
};

const main = async () => {
  const server = new Hapi.server({
    host: 'localhost',
    port: 3000
  });
  await server.register([
    require('hapi-auth-basic'),
    require('inert')
  ]);
  server.auth.strategy('simple', 'basic', {
    validate
  });
  server.auth.default('simple');

  server.route(require('./routes'));

  await server.start();
  return server;
};

Models.sequelize.sync()
.then(main()
.then((server) => console.log('Server running at:', server.info.uri))
.catch((err) => {
  console.log(err);
  process.exit(1);
}));
