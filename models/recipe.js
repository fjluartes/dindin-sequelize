'use strict';
module.exports = (sequelize, DataTypes) => {
  var Recipe = sequelize.define('Recipe', {
    name: {
      type: DataTypes.STRING,
      field: 'name'
    },
    cooking_time: {
      type: DataTypes.STRING,
      field: 'cooking_time'
    },
    prep_time: {
      type: DataTypes.STRING,
      field: 'prep_time'
    },
    serves: {
      type: DataTypes.INTEGER,
      field: 'serves'
    },
    cuisine: {
      type: DataTypes.STRING,
      field: 'cuisine'
    },
    ingredients: {
      type: DataTypes.TEXT,
      field: 'ingredients'
    },
    directions: {
      type: DataTypes.TEXT,
      field: 'directions'
    },
    stars: {
      type: DataTypes.INTEGER,
      field: 'stars'
    },
    user_id: {
      type: DataTypes.INTEGER,
      field: 'user_id'
    }
  }, {
    tableName: 'recipes',
    timestamps: false
  });
  Recipe.associate = function(models) {
    // associations can be defined here
  };
  return Recipe;
};
