'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      field: 'username'
    },
    password: {
      type: DataTypes.STRING,
      field: 'password'
    },
    token: {
      type: DataTypes.STRING,
      field: 'token'
    }
  }, {
    tableName: 'users',
    timestamps: false
  });
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};
