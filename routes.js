'use strict';
const Recipes = require('./handlers/recipes');
const Pages = require('./handlers/pages');
const Assets = require('./handlers/assets');

module.exports = [{
  method: 'GET',
  path: '/',
  handler: Recipes.home
}, {
  method: 'GET',
  path: '/recipes',
  handler: Recipes.find
}, {
  method: 'GET',
  path: '/{param*}',
  handler: Assets.servePublicDirectory
}, {
  method: 'GET',
  path: '/recipes/{id}',
  handler: Recipes.findOne
}, {
  method: 'GET',
  path: '/users',
  handler: Recipes.users
}, {
  method: 'POST',
  path: '/recipes',
  handler: Recipes.create
}, {
  method: 'POST',
  path: '/recipes/{id}/star',
  handler: Recipes.star
}];
